-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2021 at 03:34 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog_mvc`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` varchar(5000) NOT NULL,
  `author` varchar(200) NOT NULL,
  `time` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `author`, `time`, `image`) VALUES
(1, 'title00', 'content00', 'Đạt 00', '', ''),
(2, 'title01', 'content01', 'Đạt 00', '09/12/2021', 'bongda.jpg'),
(3, 'title02', 'content02', 'Đạt 00', '09/12/2021', 'about-bg.jpg'),
(4, 'title03', 'content03', 'Đạt 00', '09/12/2021', 'post-bg.jpg'),
(5, 'title04', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque finibus dui augue, quis posuere mauris maximus ut. Fusce sit amet condimentum purus. Nam commodo mi in luctus imperdiet. Sed ornare sem a consectetur hendrerit. Praesent justo urna, suscipit nec magna id, dictum iaculis enim. Sed aliquam ante nec dui interdum dignissim. Mauris vel suscipit arcu. Aliquam sed odio rutrum, laoreet odio eget, sollicitudin lorem. Fusce tempus massa eros, a congue nisl auctor nec. Phasellus pharetra metus vel urna viverra, eget efficitur lacus porttitor. Donec ut molestie est. Nunc posuere ut purus a feugiat. Nulla rhoncus risus a convallis imperdiet.\r\n\r\nIn hac habitasse platea dictumst. Maecenas euismod sapien at turpis commodo, id hendrerit lectus condimentum. Fusce at justo ex. Cras ullamcorper commodo diam. Nam eu blandit arcu, sit amet vestibulum diam. Cras at ultricies tellus, in consectetur odio. Donec accumsan odio vel ante interdum tincidunt.\r\n\r\nPhasellus consectetur erat id urna pretium, eget rhoncus erat tempor. Proin finibus risus risus, sit amet fringilla quam sollicitudin a. Vivamus id lorem a nulla mollis congue. Donec sit amet ligula nec nibh tincidunt sagittis et quis erat. Sed ac elit quis orci consectetur tincidunt nec ut nulla. Etiam a condimentum elit. Morbi eget lorem dapibus nibh dignissim aliquet.\r\n\r\nAenean pharetra, risus eu mattis laoreet, lorem nibh molestie lorem, eu maximus massa leo vitae urna. Integer pretium ipsum suscipit condimentum hendrerit. Morbi et ipsum ut nisi tempor placerat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam suscipit lectus sed malesuada malesuada. Curabitur posuere fringilla purus nec mattis. Donec eu metus fermentum, ornare purus non, hendrerit tortor. Suspendisse potenti. Maecenas faucibus eget nibh vitae commodo. Nam accumsan libero eu risus venenatis iaculis. Cras viverra vestibulum mauris, vel consectetur quam fermentum eu. Donec at metus vitae neque ornare vestibulum non et dolor. Donec non mi ut nulla accumsan varius. Pellentesque sagittis neque quis purus tincidunt tristique.\r\n\r\nVestibulum lobortis elit nec arcu pellentesque, et blandit erat feugiat. Donec velit leo, laoreet in aliquam eget, semper a justo. Vivamus tortor ante, dictum non nisl vitae, ullamcorper vestibulum neque. Maecenas elementum eu tortor nec convallis. Phasellus convallis congue nunc, vitae dapibus quam porta sit amet. Cras cursus commodo varius. Sed tincidunt erat ligula, sit amet facilisis mauris malesuada et. Cras commodo iaculis ipsum, non viverra nisi pharetra ac. Suspendisse iaculis id nibh et fermentum. Aenean ac tristique magna, at pulvinar est. Nullam vehicula nisl vel mi pharetra tristique. Aenean vehicula ultricies mi quis vestibulum. Pellentesque pellentesque quis ex ut scelerisque. Quisque sit amet turpis congue, condimentum turpis quis, malesuada lorem. Donec sagittis enim feugiat, venenatis mi viverra, tincidunt orci. Vivamus semper commodo est, ut euismod orci elementum et.', 'Đạt 00', '09/12/2021', 'post-bg.jpg'),
(6, 'title05', 'Tay trắng lập nghiệp, hắn cũng chỉ có cái danh Hán thất mơ hồ là giá trị\r\nKhởi sự không một tấc đất cắm dùi, binh ít quân kém, chạy nạn ngược xuôi, hắn bị người đời mỉa mai \"Năm lần đổi chủ, thất tán cả vợ con\". Nhưng hắn vẫn biểu lộ như nhận định \"ít nói, mừng giận không ra mặt\".\r\nĐời hắn cứ thua keo này bày keo khác, tiếp tục còn sống là còn chiến đấu cho tới ngày thành công. Kẻ anh hùng đơn giản chỉ là bền gan vững chí thêm một chút. Trang tuấn kiệt ngắn gọn cũng là sống tầm thường mà thành phi thường.\r\nVới hắn, bộc lộ thì là quá bản năng, kìm nén và đâm napoli trừ không chấm năm mới là bản lĩnh. Trong hoàn cảnh cuộc đời của hắn còn đầy gian truân, vậy mà kẻ thù lớn nhất cũng phải nói \"Thiên hạ anh hùng chỉ có Tháo này và sứ quân mà thôi\". Cũng phải, người tài không lộ tướng, mà lộ tướng thì không phải người tài!\r\nThâm tàng bất lộ thường là dị nhân chẳng biết chừng.\r\n', 'Đạt 00', '09/12/2021', 'post-sample-image.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `password`) VALUES
(1, 'user00', 'Đạt 00', 'user'),
(2, 'user01', 'Đạt 01', 'user'),
(3, 'user02', 'Đạt 02', '1'),
(4, 'user03', 'dat', '1'),
(5, 'user04', 'dat', '1'),
(6, 'user05', 'đạt', '1'),
(7, 'user06', 'đạt', '1'),
(8, 'ytr', 'yyyy', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
