<?php
require_once ROOT . '/models/user.php';

function signup_index(){
    $data = array();
    $data['template_file'] = 'index/signup.php';
    render('layout.php', $data);
}   
function signup_validate(){
    if(isPostRequest()){
        $username = $_POST['username'];
        $name = $_POST['name'];
        $password = $_POST['password'];   

        $user = new User();
        $user->get_user($name, $username, $password);
        if(!$user->getOneBy($username, 'username')) {
            $_SESSION['username'] = $username;
            $user->user_insert($user);
            redirect('index.php');
        } else {
            echo "<script> alert('Username existed!'); </script>";
            signup_index();
        }
    }
}
?>