<?php
require_once ROOT . '/models/post.php';

function index_index()
{
    $data = array();
    $data['template_file'] = 'index/index.php';
    render('layout.php', $data);
}

function post(){
    $page = page();
    $post = new Post();
    // var_dump($post->paginate($page));
    return $post->paginate($page);
}

function page(){
    $total = totalPages();
    if (!isset($_GET['p'])) {
        $pageno = 1;
    } else if($_GET['p'] > totalPages()){
        redirect('index.php');
    } else{
        $pageno = $_GET['p'];
    }
    return $pageno;
}

function totalPages(){
    $post = new Post();
    $total = $post->all();
    $total_posts = count($total);
    return ceil($total_posts / 3);
}

?>  