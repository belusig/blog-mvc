<?php
require_once ROOT . '/models/post.php';

function post_index()
{
    $data = array();
    $data['template_file'] = 'index/post.php';
    render('layout.php', $data);
}

function post_add(){
    $data = array();
    $data['template_file'] = 'index/add.html';
    render('layout.php', $data);
}

function post_post(){
    if(isPostRequest()){
        $title = $_POST['title'];
        $content = $_POST['content'];
        $image = $_POST['image'];
        $time = date("d/m/Y");
        $post = new Post();
        $post->get_post($title, $content, $image, $time);
        $post->post_insert($post);
        redirect('index.php');
    }
}