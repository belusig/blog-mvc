<?php
require_once ROOT . '/models/user.php';

function login_index(){
    $data = array();
    $data['template_file'] = 'index/login.php';
    render('layout.php', $data);
}

function login_validate(){
    if(isPostRequest()){
        $username = $_POST['username'];
        $password = $_POST['password'];
  
        $user = new User();
        $user->get_user('', $username, $password);
        $check = $user->getOneBy($username, 'username');
        if(!$check){
            echo "<script> alert('Incorrect Username!'); </script>";
            login_index();
        } else if($check['password'] != $password){
            echo "<script> alert('Password incorrect!'); </script>";
            login_index();
        } else {
            $_SESSION['username'] = $username;
            redirect('index.php');
        }
    }
}