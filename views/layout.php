<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Blog - MVC</title>
    <!-- Bootstrap core CSS -->
    <link href="/public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/public/css/mystyle.css" rel="stylesheet">
    <script src="/public/js/jquery-3.3.1.js"></script>
    <!-- Custom fonts for this template -->
    <link href="/public/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet'
        type='text/css'>
    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
        rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
    <link href="/public/css/clean-blog.min.css" rel="stylesheet">
    <!-- <link rel="shortcut icon" href="public/img/a.ico" type="image/x-icon"> -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Blog - MVC</title>
    <link rel="icon" type="image/x-icon" href="views/index/assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet"
        type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800"
        rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="views/index/css/styles.css" rel="stylesheet" />
</head>

<body>
    <!-- Navigation-->
    <?php include ROOT . DS . 'views' . DS . 'header.php'; 
        ?>
    <!-- Main Content -->
    <div class="container">
        <?php include ROOT . DS . 'views' . DS . $template_file; 
        ?>
    </div>
    <!-- Bootstrap core JavaScript -->
    <script src="/public/vendor/jquery/jquery.min.js"></script>
    <script src="/public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <?php include ROOT . DS . 'views' . DS . 'footer.php'; 
        ?>
    <!-- Custom scripts for this template -->
    <script src="/public/js/clean-blog.min.js"></script>
    <!-- Footer-->
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Core theme JS-->
    <script src="views/index/js/scripts.js"></script>
</body>

</html>