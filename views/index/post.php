<!DOCTYPE html>
<html lang="en">
    <body>
        <?php  
            $id = $_GET['id'];
            $get = new Post();
            $g = $get->getOneBy($id, 'id');
        ?>
        <!-- Page Header-->
        <header class="masthead" style="background-image: url('views/index/assets/img/<?php echo $g['image'] ?>')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="post-heading">
                            <h1><?php echo $g['title'] ?></h1>
                            <h2 class="subheading"><?php echo implode(' ', array_slice(explode(' ', $g['content']), 0, 10)); ?></h2>
                            <span class="meta">
                                Posted by <?php echo $g['author'] ?>
                                on <?php echo $g['time'] ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Post Content-->
        <article class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <?php echo $g['content'] ?>
                    </div>
                </div>
            </div>
        </article>
    </body>
</html>
