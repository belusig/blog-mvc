<!DOCTYPE html>
<html lang="en">

<body>
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('views/index/assets/img/home-bg.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <h1>Sign Up</h1>
                        <span class="subheading">A Blog Theme by Start Bootstrap</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main Content-->
    <main class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="my-5">
                        <!-- * * * * * * * * * * * * * * *-->
                        <!-- * * SB Forms Contact Form * *-->
                        <!-- * * * * * * * * * * * * * * *-->
                        <!-- This form is pre-integrated with SB Forms.-->
                        <!-- To make this form functional, sign up at-->
                        <!-- https://startbootstrap.com/solution/contact-forms-->
                        <!-- to get an API token!-->
                        <form id="contactForm" data-sb-form-api-token="API_TOKEN" method="POST"
                            action="index.php?c=signup&m=validate">
                            <div class="form-floating">
                                <input class="form-control" id="username" type="text" name="username" required
                                    placeholder="Enter your name..."/>
                                <label for="name">Username</label>
                            </div>
                            <div class="form-floating">
                                <input class="form-control" id="name" type="text" name="name" required
                                    placeholder="Enter your password..."S/>
                                <label for="name">Your Name</label>
                            </div>
                            <div class="form-floating">
                                <input class="form-control" id="password" type="password" name="password" required
                                    placeholder="Enter your password..." />
                                <label for="password">Password</label>
                            </div>
                            <br />

                            <!-- Submit error message-->
                            <!---->
                            <!-- This is what your users will see when there is-->
                            <!-- an error submitting the form-->
                            <button class="btn btn-primary text-uppercase" id="submitButton" type="submit">sign
                                up</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

</html>