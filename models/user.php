<?php 
    class User extends Model {
        public $name;
        public $username;
        public $password;
        public function get_user($name, $username, $password)
        {
            $this->name = $name;
            $this->username = $username;
            $this->password = $password;
            $this->table = 'user';
        }
        
        public function user_insert($object){
            $data = (array)$object;
            $data = (array_filter($data, function($k) {
                return $k == 'name' || $k == 'username' || $k == 'password';
            }, ARRAY_FILTER_USE_KEY));
            $this->db_insert('user', $data);
        }
 
    }
?>