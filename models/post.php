<?php

class Post extends Model{
    public $title;
    public $content;
    public $author;
    public $image;
    public $time;
    public $table = 'post';


    public function get_post($title, $content, $image, $time){
        $this->title = $title;
        $this->content = $content;
        $this->image = $image;
        $this->time = $time;
        $this->get_author();
    }

    function get_author(){
        $username = $_SESSION['username'];
        
        $sql = "SELECT * FROM user WHERE username = '$username'";
        $result = mysqli_query($this->con, $sql);

        $userr = $result->fetch_assoc();
        $this->author = $userr["name"];
        return $this->author;
    }

    public function post_insert($object){
        $data = (array)$object;
        $data = (array_filter($data, function($k) {
            return $k == 'title' || $k == 'content' 
                || $k == 'author' || $k == 'image'  || $k == 'time';
        }, ARRAY_FILTER_USE_KEY));
        
        $this->db_insert('post', $data);
    }

}